#!/usr/bin/env python3

from matplotlib.colors import LogNorm
from matplotlib.projections.polar import InvertedPolarTransform

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import argparse


def generate_az_el_histogram(data):
    # Binning of the data
    az_bins = np.linspace(0, 2*np.pi, 90)      # 0 to 2*pi in steps of 360/N.
    el_bins = np.linspace(0, 90, 30)
    theta, r = np.mgrid[0:2*np.pi:89j, 0:90:29j]
    H, xedges, yedges = np.histogram2d(np.radians(d.Azimuth),
                                       90 - d.Elevation,
                                       bins=(az_bins, el_bins))

    # Replace empty bins with 0 by np.nan, required e.g. for log plots
    H[H == 0] = np.nan

    return theta, r, H


def generate_az_el_plot(ax, theta, r, H):
    # Make a square figure
    ax.set_theta_zero_location('N')
    ax.set_theta_direction(-1)

    # Plot
    ctf = ax.contourf(theta, r, H,
                      levels=[1e0, 1e1, 1e2, 1e3],
                      cmap=plt.cm.jet,
                      norm=LogNorm())
    plt.colorbar(ctf)

    # Customize ticks and labels
    ax.set_yticks(range(0, 90+10, 10))
    yLabel = ['90°', '', '', '60°', '', '', '30°', '', '', '0°']
    ax.set_yticklabels(yLabel)
    ax.axes.tick_params(labelsize=15)


def plot_polar_scatter(ax, data):
    ax.set_theta_direction(-1)
    ax.set_theta_zero_location('N')
    ax.plot(np.radians(data.Azimuth),
            90 - data.Elevation,
            '.',
            markersize=5)

    ax.set_yticks(range(0, 90, 20))
    ax.set_yticklabels(map(str, range(90, 0, -20)))
    ax.set_rmax(90)


def horizon_helper_dialog(data, horizon_file_out):
    fig = plt.figure(figsize=(8, 8))
    ax = plt.subplot(111, projection='polar')

    plot_polar_scatter(ax, d)

    ################
    markers = {'x': [], 'y': [], 't': [], 'f': []}
    marker_line, = ax.plot([0], [0], marker='.', c='k', zorder=100)

    # TODO: Plot existing horizon mask (fill produces Polygons, which can't be edited)
    # ax.patch.set_facecolor('black')
    # marker_line, = ax.fill([0,90, 180], [90,90,90], color='white')

    def on_mouse_press(event):
        tb = plt.get_current_fig_manager().toolbar
        if not (event.button==1 and event.inaxes and tb.mode == ''):
            return

        markers['x'].append(event.xdata)
        markers['y'].append(event.ydata)

        phitheta = xy2phitheta(ax, event.xdata, event.ydata)
        print('𝜑: {:5.1f}°  𝜃: {:4.1f}°'.format(*phitheta))
        # print('x:{:.2f}  y:{:.2f} '.format(markers['x'][-1], markers['y'][-1]))
        update_plot_markers()

    def update_plot_markers():
        marker_line.set_xdata(markers['x'])
        marker_line.set_ydata(markers['y'])
        # ax.scatter(x=[int(event.xdata)], y=[int(event.ydata)], marker='.', c='k', zorder=100)
        ax.figure.canvas.draw()

    def undo_track_point_selection():
        markers['x'] = markers['x'][:-1]
        markers['y'] = markers['y'][:-1]
        update_plot_markers()

    def xy2phitheta(ax, x, y):
        xy = np.array([[x, y]])
        inverted_trafo = InvertedPolarTransform(ax)

        a,r = inverted_trafo.transform_non_affine(xy)[0]
        theta = 90.0 - r

        phi = np.rad2deg(inverted_trafo.transform_affine(xy)[0][0])
        if phi < 0:
            phi = phi % 360.0
        return phi, theta

    def write_horizon(filename, markers):
        horizon = []
        for x, y in zip(markers['x'], markers['y']):
            horizon.append(xy2phitheta(ax, x, y))

        with open(filename, 'w') as f:
            f.write('#Azimuth;Elevation\n')
            for phitheta in list(sorted(horizon, key=lambda x: x[0])):
                line = '{:5.1f};{:4.1f}\n'.format(*phitheta)
                f.write(line)

    def save_selected_track_points():
        write_horizon(horizon_file_out, markers)
        print('Custom horizon written to {}.'.format(horizon_file_out))

    def on_key(event):
        if event.key == 'u':
            undo_track_point_selection()
        elif event.key == 's':
            save_selected_track_points()

    # NOTE:Disables the 'q' keybinding as well...
    # fig.canvas.mpl_disconnect(fig.canvas.manager.key_press_handler_id)

    fig.canvas.mpl_connect('button_press_event', on_mouse_press)
    fig.canvas.mpl_connect('key_press_event', on_key)
    ################

    plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description=
        'Plot Az/El points in a polar diagram. When providing a filename to ' +
        '--horizon-out, this tool can be used to interactively define the horizon ' +
        'of the station with the mouse. ' +
        'Left-click in the polar plot to insert a new point on the horizon, ' +
        'Press \'u\' to remove the last horizon point, ' +
        'Press \'s\' to save the horizon to the specified file.'
    )
    parser.add_argument('input',
                        metavar='FILE',
                        type=argparse.FileType('r'),
                        help='A .csv FILE with header, ";" delimiters and ' +
                        'at least these columns: ' +
                        'StationID, Azimuth and Elevation')
    parser.add_argument('station_id',
                        metavar='ID',
                        type=int,
                        help='The StationID for which the plot should be ' +
                        'generated.')
    parser.add_argument('-o', '--output',
                        metavar='FILE',
                        type=argparse.FileType('w'),
                        help='Write the generated plot to FILE instead of ' +
                        'showing it interactively.')
    parser.add_argument('--scatter',
                        help='Produce a polar scatter plot instead of polar histogram',
                        action="store_true")
    parser.add_argument('--horizon-out',
                        metavar='HORIZONFILE',
                        type=str,
                        help='Activate the interactive tool to define a custom horizon. Implies [--scatter]. \n' +
                             'Write the generated horizon data to HORIZONFILE')

    args = parser.parse_args()

    # Parse the data
    data = pd.read_csv(args.input,
                       sep=";",
                       header=0,
                       index_col=None)

    # Select the desired station
    d = data.loc[data.StationID == args.station_id, ['Azimuth', 'Elevation']]

    if args.horizon_out:
        horizon_helper_dialog(d, args.horizon_out)
    else:
        fig = plt.figure(figsize=(8, 8))

        if args.scatter:
            ax = plt.subplot(111, projection='polar')

            plot_polar_scatter(ax, d)
        else:
            # Generate histogram
            theta, r, H = generate_az_el_histogram(data)

            ax = fig.add_axes([0.1, 0.1, 0.8, 0.8], polar=True, facecolor='#FFFFFF')
            generate_az_el_plot(ax, theta, r, H)

        ax.set_title('Station {} - {}'.format(args.station_id, args.input.name))

        # Save or display the plot
        if args.output:
            args.output.close()
            plt.savefig(args.output.name)
        else:
            plt.show()
