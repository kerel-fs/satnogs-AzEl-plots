#!/usr/bin/env python3

import argparse
import json
import re
import requests


from satnogs_api_client import NETWORK_BASE_URL, fetch_tle_of_observation

import settings
from utils import store_json_data, load_json_data
 

def fetch_tle_of_observation(session, observation_id, prod=True):
    base_url = (NETWORK_BASE_URL if prod else NETWORK_DEV_BASE_URL)
    url = '{}/observations/{}/'.format(base_url,
                                       observation_id)
    r = session.get(url=url)
    observation_page_html = r.text

    regex = r"<pre>1 (.*)<br>2 (.*)</pre>"
    matches = re.search(regex, observation_page_html)

    try:
        obs_tle_2 = '1 ' + matches.group(1)
        obs_tle_3 = '2 ' + matches.group(2)
    except AttributeError as e:
        print(r.text)
        raise

    return [obs_tle_2, obs_tle_3]


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Fetch missing TLEs")
    parser.add_argument("ground_station_id",
                        help="SatNOGS ground station ID")
    args = parser.parse_args()
    observations_file = settings.observations_file.format(ground_station_id=args.ground_station_id)

    obs = load_json_data(observations_file)

    session = requests.Session()

    for i, o in enumerate(obs):
        # Filter only observations with demoddata
        if not o['demoddata']:
            continue

        # Skip observations for which we already downloaded the TLEs
        if 'tle_lines' in o.keys():
            continue

        ############################################################
        # Fetch TLE for the remaining observations
        ############################################################

        print(o['id'])
        tle = fetch_tle_of_observation(session, o['id'])
        obs[i]['tle_lines'] = tle

    store_json_data(obs, observations_file)
