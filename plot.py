#!/usr/bin/env python3

from matplotlib.pyplot import rc, grid, figure, savefig
from math import radians
import csv
import argparse

parser = argparse.ArgumentParser(description='Plot Az/El points in ' +
                                             'a polar diagram')
parser.add_argument('input', metavar='INPUT', type=str, nargs='+',
                    help='A .csv file with "," delimiters of Az/El values')

args = parser.parse_args()

rc('grid', color='#316931', linewidth=1, linestyle='-')
rc('xtick', labelsize=15)
rc('ytick', labelsize=15)

# Make a square figure
fig = figure(figsize=(8, 8))
ax = fig.add_axes([0.1, 0.1, 0.8, 0.8], polar=True, facecolor='#d5de9c')
ax.set_theta_zero_location('N')
ax.set_theta_direction(-1)

# Open file and read values
with open(args.input[0], 'r') as csvfile:
    dats = csv.reader(csvfile, delimiter=',')
    dats_list = list(dats)

# Plot values
for (Az, E) in dats_list:
    ax.scatter(radians(float(Az)), 90-float(E), marker='.')

ax.set_yticks(range(0, 90+10, 10))  # Define the yticks
yLabel = ['90', '', '', '60', '', '', '30', '', '', '']
ax.set_yticklabels(yLabel)
grid(True)

# Save plotted file
savefig(args.input[0].split('.')[0]+'.png')
