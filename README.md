# SatNOGS AzEl-receiver-coverage plots

A simple python3 plotting script for Az/El values.
Based on <https://gitlab.com/snippets/1686218>.

![example plot](example_plot.png)

## Requirements

Install python requirements
```
pip install -r requirements.txt
```

## Usage

Open the az-el plot for station 2 in an interactive window.
```
./az_el_plot_log.py sats.csv 2
```

Generate an az-el plot for station 2 and save them in a file.
```
./az_el_plot_log.py sats.csv 2 -o az_el_plot_station2.png
```

### az\_el\_plot\_log.py - CLI options
```
usage: az_el_plot_log.py [-h] [-o FILE] [--scatter] [--horizon-out HORIZONFILE] FILE ID

Plot Az/El points in a polar diagram. When providing a filename to --horizon-out, this tool can be used to interactively
define the horizon of the station with the mouse. Left-click in the polar plot to insert a new point on the horizon, Press
'u' to remove the last horizon point, Press 's' to save the horizon to the specified file.

positional arguments:
  FILE                  A .csv FILE with header, ";" delimiters and at least these columns: StationID, Azimuth and Elevation
  ID                    The StationID for which the plot should be generated.

optional arguments:
  -h, --help            show this help message and exit
  -o FILE, --output FILE
                        Write the generated plot to FILE instead of showing it interactively.
  --scatter             Produce a polar scatter plot instead of polar histogram
  --horizon-out HORIZONFILE
                        Activate the interactive tool to define a custom horizon. Implies [--scatter]. Write the generated
                        horizon data to HORIZONFILE
```

## Usage - Data download scripts

Use these script to download all observations of your station, the corresponding TLEs,
and ground station data. Please note that the TLE download requires one HTTP request per
observation (there is no public API, instead the observations UI gets scraped), and thus
could cause considerable load on the SatNOGS server. Please reach out via the Chat or Forum
(<https://satnogs.org/contact/>) before you use these scripts.

These scripts create files at paths specified in `settings.py`.

```
STATION_ID=132
./1_download_observations.py 2019-01-01 $STATION_ID
./2_fetch_tles.py $STATION_ID
./3_download_ground_station.py $STATION_ID
./4_extract_pointings.py $STATION_ID "data/2019-11-22_s$STATION_ID.csv"
```

Plot the data (using the tool described above):
```
./az_el_plot_log.py data/2019-11-22_s132.csv 132
# or
./az_el_plot_log.py data/2019-11-22_s132.csv 132 --scatter
```


## Usage - daily vettings statistics

At first use the data download scripts to download observations data. Using this data the
daily vetting statistics plot can be generated with
```
./plot_day_statistics.py data/observations_132.json $STATION_ID
```
