#!/usr/bin/env python3

import argparse
from datetime import datetime
import json
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import numpy as np
import pandas as pd
from pandas.plotting import register_matplotlib_converters


register_matplotlib_converters()


# keys_vetted = df['vetted_status'].unique() , then manually sorted.
keys_vetted = ['demod', 'good', 'bad', 'unknown', 'failed']
# keys_colors = ['b', 'g', 'r', 'y', 'k']

# "official" SatNOGS color palette
keys_colors = ['b', '#449D44', '#C9302C', '#FF8C00', '#444444']


def get_day_statistics(df):
    '''
    Generate daily vetting statistics.
    df: pandas.DataFrame with observations, required columns:
        - start
        - vetted_status
        - demoddata
    '''

    # Ge
    df['demoddata_count'] = [len(d) for d in df['demoddata']]

    x = {}
    for k in keys_vetted:
        a = df[['start', 'vetted_status']].loc[df['vetted_status'] == k]

        x[k] = a.groupby(a["start"].dt.date)['start'].count()
        # Better use .asfreq here...

    a = df[df['demoddata_count'] > 0]
    x['demod'] = a.groupby(a["start"].dt.date)['start'].count()

    day_statistics = pd.DataFrame(x)
    
    # Remove 'demod' count from 'good' count since all observations with demodulated
    # data are good observations by design.
    # TODO: This is not wrong! Observations might have data and still voted as bad!
    day_statistics['good'] = day_statistics['good'].sub(day_statistics['demod'], axis=0)
    
    day_statistics.fillna(0, inplace=True)

    # Return the reindexed DataFrame
    idx = pd.date_range(sorted(day_statistics.index)[0],
                        sorted(day_statistics.index)[-1])
    return day_statistics.reindex(idx, fill_value=0)


def day_statistics_plot(day_statistics,
                        station_id,
                        roi_start,
                        roi_end,
                        stacked=True):
    fig = plt.figure(figsize=(8, 6))
    ax = plt.subplot(111)

    if stacked:
        # for k, color in zip(day_statistics.columns, keys_colors):
        ax.stackplot(day_statistics.index,
                *list([day_statistics[k] for k in keys_vetted]),
                labels=keys_vetted,
                colors=keys_colors)
    else:
        for k, color in zip(day_statistics.columns, keys_colors):
            ax.plot(day_statistics[k], '-', color=color, label=k)

    ax.set_xlim([roi_start, roi_end])
    ax.legend()
    ax.set_title('SatNOGS Station {} - Daily vetting statistics (stacked)'.format(station_id))
    ax.set_ylabel('# of Observations / day')
    ax.grid()

    fig.autofmt_xdate()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Plot daily vetting statistics for a given station.')
    parser.add_argument('OBSERVATIONS_FILE',
                        type=argparse.FileType('r'))
    parser.add_argument('station_id',
                        metavar='ID',
                        type=int,
                        help='The StationID for which the plot should be ' +
                        'generated (data filter not implemented yet!).')
    parser.add_argument('--unstacked',
                        help='Choose between "normal" and "stacked" plot style. Default: "stacked"',
                        action="store_true")

    args = parser.parse_args()

    station_id = 132
    stacked = False

    # Read observations file
    df = pd.read_json(args.OBSERVATIONS_FILE, convert_dates=['start',
                                                             'end',
                                                             'vetted_datetime',
                                                             'transmitter_updated'])

    # Extract daily vetting statistics
    day_statistics = get_day_statistics(df)

    # TODO: Extract first & last observation timestamp from data
    roi_start = datetime(2019,10,20)
    roi_end = datetime.today()

    day_statistics_plot(day_statistics,
                        args.station_id,
                        roi_start,
                        roi_end,
                        stacked=not args.unstacked)

    plt.show()
