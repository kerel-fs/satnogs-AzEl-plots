#!/usr/bin/env python3

import argparse
import json

from skyfield.api import Topos, load, EarthSatellite, Time, utc
from datetime import datetime

import settings
from utils import load_json_data


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Extract pointings")
    parser.add_argument("ground_station_id",
                        help="SatNOGS ground station ID")
    parser.add_argument("OUTFILE",
                        type=str)
    args = parser.parse_args()
    observations_file = settings.observations_file.format(ground_station_id=args.ground_station_id)
    gs_file = settings.ground_station_file.format(ground_station_id=args.ground_station_id)

    obs = load_json_data(observations_file)

    with open(gs_file) as f:
        gs = json.load(f)
    
    ts = load.timescale()
    topo = Topos(latitude_degrees=gs['lat'],
                 longitude_degrees=gs['lng'],
                 elevation_m=gs['altitude'])
    
    # Extract the demoddata timestamp from the observation
    def parse_payload_demod(s):
        return datetime.strptime(s.split('/')[-1].split('_')[-1], '%Y-%m-%dT%H-%M-%S')
        
                   
    def get_altaz(tle_lines, t):
        sat = EarthSatellite(*tle_lines)
        return (sat - topo).at(ts.utc(t.replace(tzinfo=utc))).altaz()
           
    
    ###############################################
    # Write one datapoint for each demodulated frame
    ###############################################
    outfile = open(args.OUTFILE, 'w')
    outfile.write("StationID;Azimuth;Elevation\n")
    
    for o in obs:
        for d in o['demoddata']:
            try:
                t = parse_payload_demod(d['payload_demod'])
            except:
                if(d['payload_demod'][-3:] == 'png'):
                    continue
            alt, az, d = get_altaz(o['tle_lines'], t)
            outfile.write('{};{:.1f};{:.1f}\n'.format(gs['id'], az.degrees, alt.degrees))
            
            
    outfile.close()
