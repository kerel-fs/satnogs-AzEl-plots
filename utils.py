import json


def store_json_data(data, filename):
    with open(filename, 'w') as f:
        json.dump(data, f)


def load_json_data(filename):
    with open(filename, 'r') as f:
        return json.load(f)
