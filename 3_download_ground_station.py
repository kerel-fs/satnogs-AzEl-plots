#!/usr/bin/env python3

########################################################################
# Fetch the ground station data
########################################################################
import argparse
import json

from satnogs_api_client import fetch_ground_station_data
import settings

def fetch_gs(gs_id, outfile):
    ground_stations = fetch_ground_station_data([gs_id])
    ground_station = ground_stations[0]

    with open(outfile, 'w') as f:
        json.dump(ground_station, f)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Fetch ground station data")
    parser.add_argument("ground_station_id",
                        help="SatNOGS ground station ID")
    args = parser.parse_args()

    outfile = settings.ground_station_file.format(ground_station_id=args.ground_station_id)
    fetch_gs(args.ground_station_id, outfile)
