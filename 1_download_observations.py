#!/usr/bin/env python3

import argparse
import logging

from datetime import datetime, timedelta

from satnogs_api_client.satnogs_api_client import get_paginated_endpoint
from satnogs_api_client import NETWORK_BASE_URL

import settings
from utils import store_json_data, load_json_data

logger = logging.getLogger(__name__)


def fetch_observations(start, end, ground_station_id):
    query_str = '{}/api/observations/?start={}&end={}&ground_station={}'
    
    url = query_str.format(NETWORK_BASE_URL,
                           start.isoformat(),
                           end.isoformat(),
                           ground_station_id)
    return get_paginated_endpoint(url)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Download observations")
    parser.add_argument("START",
                        help="Start date (YYYY-MM-DD)")
    parser.add_argument("ground_station_id",
                        help="SatNOGS ground station ID")

    args = parser.parse_args()

    args_start = datetime.strptime(args.START, '%Y-%m-%d')

    observations_file = settings.observations_file.format(ground_station_id=args.ground_station_id)

    logging.basicConfig(level=logging.INFO)

    logger.info("Load the metadata")
    try:
        meta = load_json_data(settings.metadata_file)
    except FileNotFoundError:
        meta = {}

    logger.info("Determine last downloaded observation datetime to optimize the following download operations")
    try:
        last_end = datetime.strptime(meta['observations_last_dt'][args.ground_station_id], '%Y-%m-%dT%H:%M:%S')
        start = max(last_end, args_start)
    except KeyError:
        meta['observations_last_dt'] = {}
        start = args_start

    end = datetime.now() - timedelta(hours=2)

    logger.info("Load old observation data")
    try:
        obs = load_json_data(observations_file)
    except FileNotFoundError:
        obs = []

    logger.info("Fetch observations")
    new_obs = fetch_observations(start, end, args.ground_station_id)

    # TODO: Find a better way to prevent duplicated observations here
    # (e.g. even a better file format than json)
    obs_ids = frozenset([o['id'] for o in obs])
    new_obs_ids = frozenset([o['id'] for o in new_obs])
    if not new_obs_ids.isdisjoint(obs_ids):
        # Fetched already observed observations.
        # Remove those observations from the old dataset
        # TODO: Re-evaluate this step!
        obs = list(filter(lambda o: o['id'] not in new_obs_ids, obs))

    logging.info("Store observations")
    store_json_data(obs + new_obs, observations_file)

    logging.info("Update metadata file")
    meta['observations_last_dt'][args.ground_station_id] = end.strftime('%Y-%m-%dT%H:%M:%S')
    store_json_data(meta, settings.metadata_file)
    logger.info("Done")
